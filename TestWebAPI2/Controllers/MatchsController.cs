﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
using System.Web.Mvc;
//=============
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI442.Models;

namespace WebAPI442.Controllers
{
    public class MatchsController   : ApiController {
 

        private mobileApp442Entities db = new mobileApp442Entities();

        // GET api/Matchs 
        public IEnumerable<Match> Get() {
            var model = db.Matches.ToList();
            return model;
        }

        // GET api/Match/5
        public Match Get(int id) {
            // return "value";
            var model = db.Matches.Where(x => x.MatchId == id )
                .FirstOrDefault();
            return model;
        }

        // GET api/ Match?id=aa&name=??
        public Match Get(int id, string name) {
            var model = db.Matches.Where(x => x.MatchId == id && x.MatchTeam1.Contains(name)).FirstOrDefault(); ;
            return model;
        }

        // GET api/ Match/GetByname?name=?? 
        public IEnumerable<Match> GetByName(string name) {
            return db.Matches.ToList();
        }

        public void Remove(int id) {
           // products.RemoveAll(p => p.Id == id);
          //  db.Matches.Remove( x => x.Id == id);
        }

 
 

    } // end class
}
